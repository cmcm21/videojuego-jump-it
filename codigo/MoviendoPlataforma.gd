extends Path2D


const VELOCIDAD = 250 
onready var ruta = $rutaAseguir 
onready var plataforma = $rutaAseguir/plataformaTierra
var direccion
var sprite_ancho_medio


func _ready():
	randomize()
	direccion = 1 if rand_range(0,100) > 50 else -1
	sprite_ancho_medio = plataforma.sprite_ancho_medio
	

func _process(delta):
	ruta.offset += delta * VELOCIDAD * direccion
	if direccion > 0 and ruta.unit_offset > 0.99:
		direccion = -1
	elif direccion < 0 and ruta.unit_offset < 0.01:
		direccion = 1



