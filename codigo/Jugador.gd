extends KinematicBody2D
##################################################
#Constantes
const GRAVEDAD = 1500
const GRAVEDAD_AUMENTADA = 2500
const DECREMENTO_BRINCO = 100
const FUERZA_BRINCO = 40

####################################################
#Variables existenes en la escena
onready var sprite_animado = $SpriteAnimado
onready var audio = $audio

#####################################################
#Variables
var anchoPantalla
var anchoMitadPantalla

var brincando = false
var fuerza_actual_de_brinco = 0
var gravedad_actual = 0
var velocidad = 500
var mas_alto = 100
var posicion_muerte = 1200
var puntuacion = 0
var rapidez = Vector2()
###################################
#señales
var centro

signal apenas_brinco

func _ready():
	puntuacion = 0
	anchoPantalla = get_viewport_rect().size.x
	anchoMitadPantalla = sprite_animado.frames.get_frame("idle",0).get_width() / 2
	gravedad_actual = GRAVEDAD
	centro = get_viewport_rect().size.x / 2


func _process(delta):
	#utilizo move_and_slide para manejar las entradas
	#touch
	move_and_slide(rapidez)
	if !brincando:
		incrementar_gravedad(delta)
		self.position.y += gravedad_actual * delta
	else:
		self.position.y -= fuerza_actual_de_brinco
		disminuir_brinco(delta)

	mas_alto  = position.y if position.y < mas_alto else mas_alto
	puntuacion = int(abs(mas_alto) - 300)
	puntuacion = puntuacion if puntuacion > 0 else  0
	if self.position.y >= mas_alto + posicion_muerte:
		muerte()

	if Input.is_action_pressed("ui_left"):
		position.x -= velocidad * delta
	elif Input.is_action_pressed("ui_right"):
		position.x += velocidad * delta

	elif Input.is_action_pressed("ui_accept"):
		brincar()
	mantener_en_pantalla()

func _input(evento):
	if (evento is InputEventScreenTouch or evento is InputEventMouseButton) and evento.is_pressed():
		if evento.position.x < centro:
			rapidez.x = -velocidad
		elif evento.position.x > centro:
			rapidez.x = velocidad
	elif (evento is InputEventScreenTouch or evento is InputEventMouseButton) and !evento.is_pressed():
		rapidez.x = 0

func mantener_en_pantalla():
	if self.position.x >= anchoPantalla:
		self.position.x = 0
	elif self.position.x <= 0:
		self.position.x = anchoPantalla


func brincar():
	if brincando:
		return
	else:
		audio.play()
		gravedad_actual = 0
		brincando = true
		fuerza_actual_de_brinco = FUERZA_BRINCO
		sprite_animado.play("brinco")
		emit_signal("apenas_brinco")

func disminuir_brinco(delta):
	fuerza_actual_de_brinco -= DECREMENTO_BRINCO * delta
	if fuerza_actual_de_brinco <= 0:
		fuerza_actual_de_brinco = 0
		brincando = false
		sprite_animado.play("idle")


func incrementar_gravedad(delta):
	gravedad_actual += GRAVEDAD_AUMENTADA * delta
	if gravedad_actual >= GRAVEDAD:
		gravedad_actual = GRAVEDAD

func muerte():
	$"/root/Datos".guardar_puntuacion(puntuacion)
	$"/root/adminNiveles".cambiar_escena("Menu")

func agregar_impulso(impulso):
	brincando = true
	fuerza_actual_de_brinco = FUERZA_BRINCO + impulso
	gravedad_actual = 0
	sprite_animado.play("idle")
	emit_signal("apenas_brinco")
	
	

