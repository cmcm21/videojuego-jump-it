extends Area2D

onready var animacion = $Animacion
onready var audio = $audio

var altura
var impulso = 100


func _ready():
	altura = animacion.frames.get_frame("Libre",0).get_height()
	connect("body_entered",self,"super_brinco")

func super_brinco(cuerpo):
	if cuerpo.name == "Jugador":
		if cuerpo.position.y < self.position.y and not cuerpo.brincando:
			audio.play()
			cuerpo.agregar_impulso(impulso)
			animacion.play("Brincolin")


