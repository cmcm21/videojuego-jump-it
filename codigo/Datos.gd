extends Node

const RUTA_DE_GUARDADO = "user://datos.save"

func guardar_puntuacion(puntuacion):
	if cargar_puntuacion() > puntuacion:
		return

	var archivoGuardar = File.new()
	archivoGuardar.open(RUTA_DE_GUARDADO,File.WRITE)
	var datos = {
			masAlto = puntuacion
		}
	archivoGuardar.store_line(to_json(datos))
	archivoGuardar.close()
	

func cargar_puntuacion():
	var archivoGuardar = File.new()
	if !archivoGuardar.file_exists(RUTA_DE_GUARDADO):
		return 0

	archivoGuardar.open(RUTA_DE_GUARDADO,File.READ)
	var datos = parse_json(archivoGuardar.get_line())
	archivoGuardar.close()
	return datos["masAlto"]

	

