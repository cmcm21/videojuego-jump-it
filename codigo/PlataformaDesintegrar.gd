extends Area2D

onready var sprite = $Sprite
onready var colision = $formaColision
onready var particulas = $particulas
onready var audio = $audio

var sprite_ancho_medio

func _ready():
	sprite_ancho_medio = sprite.texture.get_width() / 2
	connect("body_entered",self,"derrumbar")

func derrumbar(cuerpo):
	if cuerpo.name == "Jugador":
		if cuerpo.position.y < self.position.y and not cuerpo.brincando:
			cuerpo.brincar()
			audio.play()
			sprite.queue_free()
			colision.queue_free()
			particulas.emitting = true

			


