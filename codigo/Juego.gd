extends Node

export(Array) var plataformas
export(Array) var plataformas_rotas
export(Array) var plataformaEnMovimiento

#################################
#Variables Existentes
onready var jugador = $Jugador
onready var puntuacionTex = $interfaz/puntuacionTex


##############################
#Constantes
#distancia minima entre una plataforma y otra
const DISTANCIA_MIN = 100
const DISTANCIA_MAX = 200
const PLATAFORMAS_INIT = 30
const NUM_PLATAFORMAS = 5
const PORCENTAJE_PLATAFORMA_ESPECIAL = 20 
const PORCENTAJE_PLATAFORMA_MOVIBLE = 15
###############################
#Variables

var distancia_max_actual
var distancia_min_actual
var altura_de_ultima_creacion
var tam_pantalla
var indice

func _process(delta):
	puntuacionTex.text = str(jugador.puntuacion)

func _ready():
	altura_de_ultima_creacion = get_viewport().get_visible_rect().size.y
	distancia_max_actual = DISTANCIA_MIN
	distancia_min_actual = DISTANCIA_MIN
	tam_pantalla = get_viewport().get_visible_rect().size.x
	crear_primeras_plataformas()

func crear_primeras_plataformas():
# warning-ignore:unused_variable
	for i in range(PLATAFORMAS_INIT):
		crear_plataformas()



func crear_plataformas():
	randomize()
	var nueva_plataforma
	indice = rand_range(0,plataformas.size())
	
	
	if rand_range(0,100) > 100 - PORCENTAJE_PLATAFORMA_ESPECIAL:
		nueva_plataforma = plataformas_rotas[indice].instance()
	elif rand_range(0,100) > 100 - PORCENTAJE_PLATAFORMA_MOVIBLE:
		nueva_plataforma = plataformaEnMovimiento[indice].instance()
	else:
		nueva_plataforma = plataformas[indice].instance()
		
		
	add_child(nueva_plataforma)		
	var crear_x = rand_range(nueva_plataforma.sprite_ancho_medio, tam_pantalla - nueva_plataforma.sprite_ancho_medio)
	var posicion_creacion = Vector2(crear_x,altura_de_ultima_creacion)
	nueva_plataforma.position = posicion_creacion
	altura_de_ultima_creacion -= rand_range(distancia_min_actual,distancia_max_actual)
	distancia_min_actual += 5
	distancia_max_actual += 7.5
	distancia_max_actual = clamp(distancia_max_actual,DISTANCIA_MIN,DISTANCIA_MAX)
	distancia_min_actual = clamp(distancia_min_actual,DISTANCIA_MIN,DISTANCIA_MAX / 0.75)



func _on_Jugador_apenas_brinco():
# warning-ignore:unused_variable
	for i in range(NUM_PLATAFORMAS):
		crear_plataformas()
