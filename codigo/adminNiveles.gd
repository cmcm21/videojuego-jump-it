extends Node

const RUTA_ESCENAS = "res://escenas/"

  

func cambiar_escena(nombre_escena):
	call_deferred("_cambiar_escena_deferred",nombre_escena)


func _cambiar_escena_deferred(nombre_escena):
	var ruta = RUTA_ESCENAS + nombre_escena + ".tscn"
	var raiz = get_tree().get_root()
	var actual = raiz.get_child(raiz.get_child_count() - 1)
	actual.free()
	var escena_fuente = ResourceLoader.load(ruta)
	var nueva_escena = escena_fuente.instance()
	get_tree().get_root().add_child(nueva_escena)
	get_tree().set_current_scene(nueva_escena)

	
