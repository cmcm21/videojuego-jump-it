extends Area2D

onready var sprite = $Sprite
onready var visibilidad = $visibilidad

const PORCENTAJE_DE_APARICION_BRINCOLIN = 5 
var ruta_brincolin = "res://escenas/Brincolin.tscn"

var sprite_ancho_medio

func _ready():
	randomize()
	connect("body_entered",self , "pisar_plataforma")
	sprite_ancho_medio = sprite.texture.get_size().x/2 * scale.x
	if rand_range(0,100) > 100 - PORCENTAJE_DE_APARICION_BRINCOLIN:
		var nuevo_brincolin = load(ruta_brincolin).instance()
		add_child(nuevo_brincolin)
		nuevo_brincolin.position = Vector2(0,-nuevo_brincolin.altura)
	


func pisar_plataforma(body):
	if body.name == "Jugador":
		if body.position.y < self.position.y:
			body.brincar()
		


func _on_visibilidad_screen_exited():
	queue_free()
	

